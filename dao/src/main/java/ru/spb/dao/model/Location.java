package ru.spb.dao.model;

import lombok.Data;

@Data
public class Location {
    private long id;
    private String slug;
    private String name;
    private String timezone;
    private Coords coords;
    private String language;
    private String currency;
}
