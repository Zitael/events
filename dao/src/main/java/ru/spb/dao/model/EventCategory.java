package ru.spb.dao.model;

import lombok.Data;

@Data
public class EventCategory {
    private long id;
    private String slug;
    private String name;
}
