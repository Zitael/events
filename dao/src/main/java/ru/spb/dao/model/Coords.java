package ru.spb.dao.model;

import lombok.Data;

@Data
public class Coords {
    private double lat;
    private double lon;
}
