package ru.spb.dao.model;

import lombok.Data;

@Data
public class Event {
    private long id;
    private long publication_date;
    private Dates dates;
    private String title;
    private Place place;
    private String description;
    private String body_text;
    private Location location;
    private Category[] categories;
    private String tagline;
    private String age_restriction;
    private String price;
    private boolean is_free;
    private String[] imagesNOTWORK;
    private long favorite_count;
    private long comments_count;
    private String site_url;
    private String short_title;
    private String[] tags;
    private boolean disable_comments;
    private Participant[] participantsNOTWORK;

    @Data
    private class Dates {
        private long start;
        private long end;
    }
}
