package ru.spb.dao.model;

import lombok.Data;

@Data
public class Place {
    private long id;
    private String title;
    private String slug;
    private String address;
    private String phone;
    private boolean is_stub;
    private String site_url;
    private Coords coords;
    private String subway;
    private boolean is_closed;
    private Location location;
}
