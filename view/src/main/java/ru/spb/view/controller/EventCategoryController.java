package ru.spb.view.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.spb.dao.model.EventCategory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

@RestController
@RequestMapping("/event-categories")
public class EventCategoryController {
    @GetMapping
    String find(@RequestParam String lang,
                @RequestParam String order_by,
                @RequestParam String fields) throws IOException {
        String url = "https://kudago.com/public-api/v1.4/event-categories/?lang="+
                lang+"&order_by="+order_by+"&fields="+fields;

        URL obj = new URL(url);
        HttpURLConnection connection = (HttpURLConnection) obj.openConnection();
        connection.setRequestMethod("GET");
        BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String inputLine;
        StringBuilder response = new StringBuilder();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        ObjectMapper mapper = new ObjectMapper();
        List<EventCategory> myObjects = mapper.readValue(response.toString(), new TypeReference<List<EventCategory>>(){});
        for (EventCategory c: myObjects) {
            System.out.println(c);
        }
        return response.toString();
}
}
